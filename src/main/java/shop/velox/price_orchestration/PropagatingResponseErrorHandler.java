package shop.velox.price_orchestration;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;

@Component
public class PropagatingResponseErrorHandler extends DefaultResponseErrorHandler {

  private static final Logger LOG = LoggerFactory.getLogger(PropagatingResponseErrorHandler.class);

  @Override
  protected void handleError(ClientHttpResponse response, HttpStatus statusCode)
      throws IOException {
    LOG.debug("Received {}", statusCode);
  }
}
