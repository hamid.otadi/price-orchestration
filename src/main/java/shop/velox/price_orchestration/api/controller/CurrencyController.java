package shop.velox.price_orchestration.api.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.SortDefault;
import org.springframework.data.web.SortDefault.SortDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Mono;
import shop.velox.price.api.dto.CurrencyDto;

@Tag(name = "Currency", description = "the Currency API")
@RequestMapping("")
public interface CurrencyController {

  String CURRENCIES_PATH = "/currencies";
  @Operation(summary = "Get Currencies, sorted by isoCode ascending")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
  })
  @GetMapping(value = CURRENCIES_PATH)
  Mono<ResponseEntity<String>> getCurrencies(
      @SortDefaults({
          @SortDefault(sort = "isoCode", direction = Direction.ASC)}) Pageable pageable
  );


  @Operation(summary = "Get currency")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = CurrencyDto.class))),
  })
  @GetMapping(value = CURRENCIES_PATH + "/{id}")
  Mono<ResponseEntity<CurrencyDto>> getCurrency(
      @Parameter(description = "ID of the currency. Cannot be empty.", required = true) @PathVariable("id") final String id
  );
  
}
