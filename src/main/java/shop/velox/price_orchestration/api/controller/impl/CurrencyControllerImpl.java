package shop.velox.price_orchestration.api.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import shop.velox.price.api.dto.CurrencyDto;
import shop.velox.price_orchestration.api.controller.CurrencyController;
import shop.velox.price_orchestration.service.PriceOrchestrationService;

@RestController
public class CurrencyControllerImpl implements CurrencyController {

  private static final Logger LOG = LoggerFactory.getLogger(CurrencyControllerImpl.class);

  private PriceOrchestrationService priceOrchestrationService;

  public CurrencyControllerImpl(@Autowired PriceOrchestrationService priceOrchestrationService) {
    this.priceOrchestrationService = priceOrchestrationService;
  }


  @Override
  public Mono<ResponseEntity<String>> getCurrencies(Pageable pageable) {
    return Mono.fromSupplier(() -> priceOrchestrationService.getCurrencies(pageable));
  }

  @Override
  public Mono<ResponseEntity<CurrencyDto>> getCurrency(String id) {
    return Mono.fromSupplier(() -> priceOrchestrationService.getCurrency(id));
  }

}
