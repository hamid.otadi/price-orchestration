package shop.velox.price_orchestration.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Used to indicate that a resource cannot be created because it already exists
 */
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Resource already exists")
public class ConflictException extends ClientErrorException {

  private static final Response.Status STATUS = Response.Status.CONFLICT;

  public ConflictException() {
    super(STATUS.getStatusCode());
  }

  public ConflictException(final String message) {
    super(message, STATUS);
  }
}
