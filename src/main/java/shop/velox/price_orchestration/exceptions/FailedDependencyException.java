package shop.velox.price_orchestration.exceptions;

import javax.ws.rs.ClientErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Used to indicate that the request failed because it depended on another request and that request
 * failed
 */
@ResponseStatus(value = HttpStatus.FAILED_DEPENDENCY, reason = "Other required request failed")
public class FailedDependencyException extends ClientErrorException {

  public static final HttpStatus STATUS = HttpStatus.FAILED_DEPENDENCY;

  public FailedDependencyException() {
    super(STATUS.value());
  }

  public FailedDependencyException(final String message) {
    super(message, STATUS.value());
  }
}
