package shop.velox.price_orchestration.exceptions;

import javax.ws.rs.ClientErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Used to indicate that not all required parameters have been provided, or that they are invalid.
 */
@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Missing or Invalid Required information")
public class InvalidDataException extends ClientErrorException {

  public static final HttpStatus STATUS = HttpStatus.UNPROCESSABLE_ENTITY;

  public InvalidDataException() {
    super(STATUS.value());
  }

  public InvalidDataException(final String message) {
    super(message, STATUS.value());
  }
}
